### 过滤器说明

这是一个仅适用于Adguard的自用规则，只作为AdGuard基础过滤器、AdGuard防跟踪保护过滤器和AdGuard中文过滤器一起搭配使用时的补充规则使用，不适用于Android版应用。

### 过滤器订阅地址

https://gitlab.com/lodepuly/adguardfilter/-/raw/main/Filter/filter.txt

### 订阅方法

打开“设置”→点击“广告过滤器”→点击“添加过滤器”→点击“导入过滤器”→填写过滤器地址→勾选“受信任的过滤器”→点击“安装”
